namespace App\Http\Middleware;

use Closure;

class VerifyToken extends Middleware
{
  /**
   * Обработка входящего запроса
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next)
  {
    if(!Auth::guard('api')->check())
    { 
        return response()->json(['message' => 'Error']);
    }
    return $next($request);
  }
}
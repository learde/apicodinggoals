<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Goal;
use Auth;
use Cloudinary;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\Post;
use App\Models\Comment;


class GoalController extends Controller
{

    //-------------------------------CRUD--------------------------------------------

    /**
     * Create a goal
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        if (!Auth::guard('api')->check()) {
            return response()->json(['message' => 'Error'], 401);
        }

        $g = Goal::orderBy('id', 'desc')->first();
        if ($g == null) $id = 1;
        else $id = $g->id + 1;


        $comm = GoalController::convert_str_to_bool($request->isCommentable);
        $priv = GoalController::convert_str_to_bool($request->isPrivate);

        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'endCriterion' => 'required|string',
            'description' => 'string|nullable',
            'todo' => 'required|json',
            'repo' => 'active_url|nullable',
            'category' => 'string'
        ]);


        if($validator->fails())
        {
            return response()->json([
                'message' => 'Invalid data',
            ], 404);
        }

        if (isset($request->image))
            $image = $request->image->storeOnCloudinary('apicodinggoals/avatars/goals', $id)->getPath();
        else $image = 'https://res.cloudinary.com/dme9p3izd/image/upload/v1648937213/apicodinggoals/avatars/goals/pe6f1qqpiiaqetevh423.png';


        $goal = Goal::create(array_merge(
            $validator->validated(),
            ['id' => $id,
                'user_id' => Auth::id(),
                'image' => $image,
                'isPrivate' => $priv,
                'isCommentable' => $comm,
                'isFinished' => 0,
            ]
        ));

        return $goal;
    }



    /**
     * Edit a goal
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function Edit(Request $request, $id)
    {
        if (!Auth::guard('api')->check()) {
            return response()->json(['message' => 'Error'], 401);
        }

        $validator = Validator::make($request->all(), [
            'title' => 'string',
            'endCriterion' => 'string',
            'description' => 'string',
            'todo' => 'json',
            'isPrivate' => 'string',
            'isCommentable' => 'string',
            'repo' => 'active_url|nullable',
            'isFinished' => 'string',
            'category' => 'string',
        ]);

        $goal = Goal::where('user_id', Auth::id())->where('id', $id)->first();

        $goal->fill(array_merge(
            $validator->validated(),
        ));

        $goal = GoalController::edit_image($goal, $request);

        $goal->save();

        return response()->json([
            $goal,
            'message' => 'Goal has been modified successfully',
        ], 201);

    }



    /**
     * Delete a goal
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    static public function delete($id)
    {
        $goal = Goal::where('id', $id)->first();

        if ($goal->image != 'https://res.cloudinary.com/dme9p3izd/image/upload/v1648937213/apicodinggoals/avatars/goals/pe6f1qqpiiaqetevh423.png') {
            preg_match_all('/^.+\/(apicodinggoals.+)\..*$/', $goal->image, $path);
            ImageUploadController::destroy_image($path[1]);
        }

        $arr = Post::where('goal_id', $goal->id)->get();

        foreach ($arr as $k => $val) {
            // $comm = Comment::where('post_id', $val['id'])->get();
            // foreach ($comm as $k1 => $val1) {
            //     CommentController::delete($val1['id']);
            // }

            PostController::delete($val['id']);
        }

        if ($goal === null) {
            return response()->json([
                'message' => 'The goal does not exist',
            ], 201);
        } else {
            $goal->delete();
            return response()->json([
                'message' => 'Goal deleted successfully',
            ], 201);
        }
    }

    //-------------------------------CRUD--------------------------------------------




    //-------------------------------Getting goals--------------------------------------------

    /**
     * Get all goals
     * @param $limit
     * @return mixed
     */
    public function getAll($limit)
    {
        $goals = Goal::where('isPrivate', false)->orderBy('created_at', 'DESC')->paginate($limit);

        $goals->transform(function ($item) {
            return collect([
                'goal' => $item,
                'user' => User::where('id', $item->user_id)->first()
            ]);
        });
        return $goals;
    }


    /**
     * Searching a goal
     *
     * @param Request $request
     * @param int $limit
     * @return LengthAwarePaginator
     */
    public function search(Request $request, $limit = 5)
    {

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $goals = Goal::all();
        $arr = [];
        foreach ($goals as $g) {
            if (str_contains(strtolower($g->title), strtolower($request->title))) {
                array_push($arr, collect([
                    'goal' => $g,
                    'user' => User::where('id', $g->user_id)->first()
                ]));
            }
        }
        $count = count($arr);
        $arr = array_slice($arr, $limit * ($currentPage - 1), $limit);
        $pag = new LengthAwarePaginator($arr, $count, $limit, 1);
        return $pag;
    }


    /**
     * Get all user's goal
     *
     * @param $limit
     * @return mixed
     */
    public function getOneUserGoals($limit)
    {
        return auth()->user()->goals()->orderBy('created_at', 'DESC')->paginate($limit);
    }


    /**
     * Get all user's goal by id
     *
     * @param $user_id
     * @param $limit
     * @return mixed
     */
    public function getOneUserGoals_id($user_id, $limit)
    {
        return User::find($user_id)->goals()->orderBy('created_at', 'DESC')->paginate($limit);
    }


    /**
     * Get one goal by id
     *
     * @param $id
     * @return array
     */
    public function getOne($id)
    {
        $goal = Goal::all()->where('id', $id)->first();
        $user_info = User::where('id', $goal->user_id)->first();

        return array_merge([
            $goal,
            $user_info
        ]);
    }

    //-------------------------------Getting goals--------------------------------------------




    //-------------------------------Favorites--------------------------------------------

    /**
     * Add goal to favorite
     *
     * @param $goal_id
     * @return mixed
     */
    public function add_to_favorites($goal_id)
    {
        $user = Auth::user();
        $fav = $user->favorites;

        if (!in_array($goal_id, $fav)) {
            array_push($fav, $goal_id);
            $user->favorites = $fav;
            $user->save();
        }

        return $fav;
    }

    /**
     * Delete goal from favorites
     *
     * @param $goal_id
     * @return mixed
     */
    public function del_from_favorites($goal_id)
    {
        $user = Auth::user();

        $fav = $user->favorites;
        if (in_array($goal_id, $fav)) {
            $key = array_search($goal_id, $fav);
            unset($fav[$key]);
            $user->favorites = $fav;
            $user->save();
        }

        return $fav;
    }

    /**
     * Get all user's favorite goals
     *
     * @return array
     */
    public function all_favorites()
    {
        $fav = Auth::user()->favorites;

        $arr = [];
        foreach ($fav as $g) {
            $goal = Goal::where('id', $g)->first();
            array_push($arr, collect([
                'goal' => $goal,
                'user' => User::where('id', $goal->user_id)->first()
            ]));

        }
        return $arr;
    }

    //-------------------------------Favorites--------------------------------------------





    //-------------------------------Helpers--------------------------------------------

    /**
     * Create n goals(for Testing)
     *
     * @param Request $request
     * @param $count
     * @return \Illuminate\Http\JsonResponse
     */
    public function create_n_goals(Request $request, $count)
    {

        for ($i = 0; $i < $count; $i++) {
            $this->create($request);
        }
        return response()->json([
            'message' => 'Test Goals has deen created',
        ], 201);


    }


    /**
     * Edit goal images
     *
     * @param $goal
     * @param $request
     * @return mixed
     */
    public function edit_image($goal, $request)
    {
        if (isset($request->image))
            $image = $request->image->storeOnCloudinary('apicodinggoals/avatars/goals')->getPath();
        else $image = $goal->image;

        if (isset($request->image) && $goal->image != 'https://res.cloudinary.com/dme9p3izd/image/upload/v1648937213/apicodinggoals/avatars/goals/pe6f1qqpiiaqetevh423.png') {
            preg_match_all('/^.+\/(apicodinggoals.+)\..*$/', $goal->image, $path);
            Cloudinary::destroy($path[1]);
        }
        $goal->image = $image;

        return $goal;
    }


    /**
     * Convert string to bool
     *
     * @param $string
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function convert_str_to_bool($string)
    {
        if ($string == "true" || $string == "1") return true;
        else if ($string == "false" || $string == "0") return false;
        else return response()->json(['message' => 'Invalid boolean field']);


    }

    //-------------------------------Helpers--------------------------------------------
}

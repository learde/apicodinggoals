<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Comment;
use Auth;
use App\Models\User;
use App\Models\Goal;
use App\Http\Controllers\CommentController;

class PostController extends Controller
{

    //---------------------------------------------CRUD--------------------------------------------

    /**
     * Create Post
     *
     * @param Request $request
     * @param $goal_id
     * @return mixed
     */
    public function create(Request $request, $goal_id)
    {
        $g = Post::orderBy('id', 'desc')->first();

        if ($g == null) $id = 1;
        else $id = $g->id + 1;

        return Post::create([
            'id' => $id,
            'text' => $request->text,
            'goal_id' => $goal_id,
        ]);
    }


    /**
     * Delete Post
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    static public function delete($id)
    {

        $post = Post::find($id);

        $comm = Comment::where('post_id', $id)->get();
        foreach ($comm as $k1 => $val1) {
            CommentController::delete($val1['id']);
        }

        if ($post === null) {
            return response()->json([
                'message' => 'The post does not exist',
            ], 201);
        } else {
            $post->delete();
            return response()->json([
                'message' => 'Post deleted successfully',
            ], 201);
        }
    }


    /**
     * Edit Post
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function Edit(Request $request, $id)
    {
        if (!Auth::guard('api')->check()) {
            return response()->json(['message' => 'Error'], 401);
        }


        $validator = Validator::make($request->all(), [
            'text' => "string"
        ]);

        $post = Goal::find($id);


        $post->fill(array_merge(
            $validator->validated(),
        ));

        $post->save();

        return response()->json([
            $post,
            'message' => 'Goal has been modified successfully',
        ], 201);

    }

    //---------------------------------------------CRUD--------------------------------------------




    /**
     * Get all Posts
     *
     * @param $goal_id
     * @param $limit
     * @return mixed
     */
    public function get_posts($goal_id, $limit)
    {
        $posts = Post::where('goal_id', $goal_id)->orderBy('created_at', 'DESC')->paginate($limit);

        return $posts;
    }




    //---------------------------------------------Comments--------------------------------------------

    /**
     * Leave a comment on Post
     *
     * @param Request $request
     * @param $post_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function comment(Request $request, $post_id)
    {
        $comment1 = CommentController::make_comment($request->text, $post_id);
        return $comment1;
    }

    /**
     * Get all Post's comment
     *
     * @param $post_id
     * @param $limit
     * @return mixed
     */
    public function get_comments($post_id, $limit)
    {
        $comments = Comment::where('post_id', $post_id)->orderBy('created_at', 'DESC')->paginate($limit);

        $comments->transform(function ($comment) {
            return collect([
                'comment' => $comment,
                'user' => User::find($comment->user_id),
            ]);
        });

        return $comments;
    }

    //---------------------------------------------Comments--------------------------------------------





    //---------------------------------------------Likes--------------------------------------------

    /**
     * Set like on Post
     *
     * @param $post_id
     * @return mixed
     */
    public function like($post_id)
    {
        $user = Auth::user();
        $post = Post::find($post_id);
        $likes = $post->likes;
        $flag = false;

        foreach ($likes as $k => $val) {
            if ($val['id'] == $user->id)
                $flag = true;
        }

        if (!$flag) {
            array_push($likes, $user);
            $post->likes = $likes;
            $post->save();
        }

       // dd($post);

        $user2 = User::find(Goal::find($post->goal_id)->user_id);

        $user2->likes += 1;

        $user2->save();


        return $post;
    }


    /**
     * Delete like from Post
     *
     * @param $post_id
     * @return mixed
     */
    public function unlike($post_id)
    {
        $user = Auth::user();
        $post = Post::find($post_id);
        $likes = $post->likes;


        foreach ($likes as $k => $val) {
            if ($val['id'] == $user->id)
                unset($likes[$k]);
        }

        $post->likes = $likes;
        $post->save();

        $user2 = User::find(Goal::find($post->goal_id)->user_id);
        $user2->likes -= 1;
        $user2->save();

        return $likes;

    }


    /**
     * Get all Post's likes
     *
     * @param $post_id
     * @return mixed
     */
    public function get_likes($post_id)
    {
        return Post::find($post_id)->likes;
    }

    //---------------------------------------------Likes--------------------------------------------

}

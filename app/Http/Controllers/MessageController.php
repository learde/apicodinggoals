<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Goal;
use Validator;
use Auth;
use App\Models\Message;
use App\Models\User;
use DateTime;

class MessageController extends Controller
{

    public function webhook()
    {
        $file = "people3.txt";
        file_put_contents($file, 4, FILE_APPEND | LOCK_EX);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $user_id)
    {
        $user = Auth::user();
        $user2 = User::find($user_id);


        // if(!(in_array($user_id, $user->subscriptions) && in_array($user->id, $user2->subscriptions)))
        // {
        //     return response()->json([
        //         'message' => 'You can send messages only to friends',
        //     ], 404);
        // }
        $validator = Validator::make($request->all(), [
            'message' => 'required|string',
        ]);

        if($validator->fails())
        {
            return response()->json([
                'message' => 'Invalid data',
            ], 404);
        }


        $goal = Message::create(array_merge(
            $validator->validated(),
            [
                'sender_id' => Auth::id(),
                'recipient_id' => $user_id
            ]
        )
        );



        return response()->json([
            $goal,
            'message' => 'Successfully sended',
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_messages($user_id)
    {
        $messages1 = Message::where('sender_id', Auth::id())->where('recipient_id',  $user_id)->get();

        $messages2 = Message::where('sender_id', $user_id)->where('recipient_id',  Auth::id())->get();



        return response()->json([
            'sender' => $messages1,
            'recipient' => $messages2
        ], 201);


    }



    public function show_dialogs()
    {
        $messages1 = Message::where('sender_id', Auth::id())->get();
        $messages2 = Message::where('recipient_id', Auth::id())->get();
        $arr = [];

        $messages = $messages1->merge($messages2);

        foreach ($messages as $message)
        {

            $user2id = $message->sender_id == Auth::id() ? $message->recipient_id : $message->sender_id;

            if (!in_array($user2id, $arr))
            {

                $user = User::find($message->recipient_id);
                if($user->id == Auth::id()) $user = User::find($message->sender_id);
                $message2 = Message::where('sender_id', $user2id)->where('recipient_id', Auth::id())->orderBy('created_at', 'desc')->first();
                $message1 = Message::where('recipient_id', $user2id)->where('sender_id', Auth::id())->orderBy('created_at', 'desc')->first();
                $time1 = $message1 ? $message1->created_at : new DateTime('2011-01-01T15:03:01.012345Z');
                $time2 = $message2 ? $message2->created_at : new DateTime('2011-01-01T15:03:01.012345Z');
                $messagec1 = $time1 > $time2 ? $time1 : $time2;
                $messagec = Message::where('created_at', $messagec1)->first();
                $arr[$user2id] =
                [
                    'user_name' => $user->name,
                    'avatar' => $user->avatar,
                    'last_message' => $messagec->message,
                    'time' => $messagec->created_at,
                    'sender_name' => User::find($messagec->sender_id)->name,
                ];
            }
        }

        //dd(!in_array($message->recipient_id, $arr));

        return $arr;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if (!Auth::guard('api')->check()) {
            return response()->json(['message' => 'Error'], 401);
        }

        $validator = Validator::make($request->all(), [
            'message' => 'string',
        ]);

        if($validator->fails())
        {
            return response()->json([
                'message' => 'Invalid data',
            ], 404);
        }


        $message = Message::find($id);

        $message->fill(array_merge(
            $validator->validated(),
        ));
        $message->save();

        return response()->json([
            $message,
            'message' => 'Message has been modified successfully',
        ], 201);

    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    static public function delete($id)
    {
        $message = Message::find($id);

        if($message)
        {
            $message->delete();
            return  response()->json([
                'message' => 'message successfully deleted',
            ], 201);
        }
        else
        {
            return  response()->json([
                'message' => 'Cannot delete message',
            ], 404);
        }
    }
}

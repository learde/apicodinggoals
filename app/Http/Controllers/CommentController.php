<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\Comment;
use Illuminate\Http\Request;
use Validator;
use App\Models\User;


class CommentController extends Controller
{
    //----------------------------------------------CRUD--------------------------------------------

    /**
     * Create a comment
     *
     * @param $comment
     * @param $post_id
     * @return \Illuminate\Http\JsonResponse
     */
    public static function make_comment($comment, $post_id)
    {
        if (!Auth::guard('api')->check()) {
            return response()->json(['message' => 'Error'], 401);
        }
        $g = Comment::orderBy('id', 'desc')->first();


        if ($g == null) $id = 1;
        else $id = $g->id + 1;


        return Comment::create(
            [
                'id' => $id,
                'text' => $comment,
                'post_id' => $post_id,
                'user_id' => Auth::id(),
            ]);
    }


    /**
     * Edit comment
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function Edit(Request $request, $id)
    {
        if (!Auth::guard('api')->check()) {
            return response()->json(['message' => 'Error'], 401);
        }


        $validator = Validator::make($request->all(), [
            'text' => "string"
        ]);

        $comm = Comment::find($id);


        $comm->fill(array_merge(
            $validator->validated(),
        ));

        $comm->save();

        return response()->json([
            $comm,
            'message' => 'Goal has been modified successfully',
        ], 201);

    }


    /**
     * Delete comment
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    static public function delete($id)
    {
        $comm = Comment::where('user_id', Auth::id())->find($id);
        if ($comm === null) {
            return response()->json([
                'message' => 'The comment does not exist',
            ], 201);
        } else {
            $comm->delete();
            return response()->json([
                'message' => 'Comment deleted successfully',
            ], 201);
        }
    }

    //----------------------------------------------CRUD--------------------------------------------



    //----------------------------------------------Likes--------------------------------------------

    /**
     * Set a like to comment
     *
     * @param $comm_id
     * @return mixed
     */
    public function like($comm_id)
    {
        $user = Auth::user();
        $comm = Comment::find($comm_id);
        $likes = $comm->likes;
        $flag = false;

        foreach ($likes as $k => $val) {
            if ($val['id'] == $user->id)
                $flag = true;
        }

        if (!$flag) {
            array_push($likes, $user);
            $comm->likes = $likes;
            $comm->save();
        }

        $user2 = User::find($comm->user_id);

        $user2->likes += 1;

        $user2->save();


        return $comm;
    }


    /**
     * Delete a like from comment
     *
     * @param $comm_id
     * @return mixed
     */
    public function unlike($comm_id)
    {
        $user = Auth::user();

        $comm = Comment::find($comm_id);
        $likes = $comm->likes;

        foreach ($likes as $k => $val) {
            if ($val['id'] == $user->id)
                unset($likes[$k]);
        }

        $comm->likes = $likes;
        $comm->save();

        $user2 = User::find($comm->user_id);

        $user2->likes -= 1;

        $user2->save();


        return $likes;

    }


    /**
     * Get all Comment's likes
     *
     * @param $comm_id
     * @return mixed
     */
    public function get_likes($comm_id)
    {
        return Comment::find($comm_id)->likes;
    }

    //----------------------------------------------Likes--------------------------------------------
}

<?php

namespace App\Http\Controllers;

use App\Models\User;
use Auth;
use Cloudinary;
use Validator;
use Illuminate\Http\Request;



class MainController extends Controller
{
    public function GetData()
    {
        return auth()->user();
    }

    

    //----------------------------------------Profile----------------------------------------

    /**
     * Edit user profile
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function EditProfile(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'country' => 'string',
            'birthday' => 'date',
            'NewPassword' => 'string|min:8',
            'gender' => 'string',
            'town' => 'string',
            'patro' => 'string',
            'surname' => 'string',
            'name' => 'string',
            'email' => 'string',
            'git' => 'active_url|nullable',
            'description' => 'string|nullable',
        ]);

        if (!Auth::guard('api')->check())
            return response()->json(['message' => 'Error'], 401);

        $user = Auth::user();


        
        $user = MainController::edit_photos($user, $request);
        //return $user;


        $user->fill(array_merge(
            $validator->validated(),
        ));

        $user->save();


        return response()->json([
            $user,
            'message' => 'Profile has been modified successfully',
        ], 201);

    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile()
    {
        if (!Auth::guard('api')->check()) {
            return response()->json(['message' => 'Error'], 401);
        } else return response()->json(auth()->user(), 200);
    }


    public function del_user($user_id)
    {
        $user = User::find($user_id);
        //dd($user);
        $goals = $user->goals()->get();
        //return $goals;
        foreach($goals as $goal) 
        {
            GoalController::delete($goal->id);
        }
        $comments = $user->comments()->get();
        foreach($comments as $comment) 
        {
            CommentController::delete($comment->id);
        }
        
        $messages = $user->messages()->get();
        //dd($messages);
        foreach($messages as $message) 
        {
            MessageController::delete($message->id);
        }

        $user->delete();
        return response()->json([
            'message' => 'User successfully deleted',
            'user' => $user
        ], 201);
    }

    //----------------------------------------Profile----------------------------------------




    //----------------------------------------Subscriptions----------------------------------------

    /**
     * Add user to subscriptions
     *
     * @param $user_id
     * @return int
     */
    public function add_to_subscriptions($user_id)
    {
        $user = Auth::user();

        $user2 = User::where('id', $user_id)->first();
        if (!$user2) return 0;

        $sub = $user->subscriptions;

        if (!in_array($user_id, $sub)) {
            array_push($sub, $user_id);
            $user->subscriptions = $sub;
            $user->save();
        }

        $sub2 = $user2->subscribers;

        if (!in_array($user->id, $sub2)) {
            array_push($sub2, $user->id);
            $user2->subscribers = $sub2;
            $user2->save();
        }



        return $sub;
    }


    /**
     * Delete user from subscriptions
     *
     * @param $user_id
     * @return mixed
     */
    public function del_from_subscriptions($user_id)
    {
        $user = Auth::user();

        $user2 = User::where('id', $user_id)->first();
        if (!$user2) return 0;

        $sub = $user->subscriptions;
        if (in_array($user_id, $sub)) {
            $key = array_search($user_id, $sub);
            unset($sub[$key]);
            $user->subscriptions = $sub;
            $user->save();
        }

        $sub2 = $user2->subscribers;
        if (in_array($user->id, $sub2)) {
            $key = array_search($user->id, $sub2);
            unset($sub2[$key]);
            $user2->subscribers = $sub2;
            $user2->save();
        }

        return $sub;
    }


    /**
     * Get all subscriptions
     *
     * @return array
     */
    public function all_subscriptions()
    {
        $fav = Auth::user()->subscriptions;

        $arr = [];
        foreach ($fav as $g) {
            $goal = User::where('id', $g)->first();
            array_push($arr, $goal);

        }
        return $arr;
    }


     /**
     * Get all subscriptions
     *
     * @return array
     */
    public function all_subscribers()
    {
        $fav = Auth::user()->subscribers;

        $arr = [];
        foreach ($fav as $g) {
            $goal = User::where('id', $g)->first();
            array_push($arr, $goal);

        }
        return $arr;
    }

    //----------------------------------------Subscriptions----------------------------------------




    //----------------------------------------Helpers----------------------------------------

    /**
     * Get user by id(Test)
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_user_by_id($id)
    {
        return response()->json(User::where('id', $id)->first(), 200);
    }

    /**
     * Edit photos(Test)
     *
     * @param $user
     * @param $request
     * @return mixed
     */
    public function edit_photos($user, $request)
    {
        if (isset($request->avatar))
            $avatar = $request->avatar->storeOnCloudinary('apicodinggoals/avatars/users', $user->id)->getPath();
        else $avatar = $user->avatar;

        if (isset($request->background))
            $background = $request->background->storeOnCloudinary('apicodinggoals/backgrounds', $user->id)->getPath();
        else $background = $user->background;


        if (isset($request->avatar) && $user->avatar != 'http://res.cloudinary.com/dme9p3izd/image/upload/v1648404630/apicodinggoals/avatars/users/idw5p3a8nmpx0issug7z.webp') {
            preg_match_all('/^.+\/(apicodinggoals.+)\..*$/', $user->avatar, $path);
            
            Cloudinary::destroy($path[1]);
        }


        if (isset($request->background) && $user->background != 'http://res.cloudinary.com/dme9p3izd/image/upload/v1648404126/apicodinggoals/backgrounds/defaults/ulxxheiu8emsho3aa3g4.jpg') {
            preg_match_all('/^.+\/(apicodinggoals.+)\..*$/', $user->background, $path);
            Cloudinary::destroy($path[1]);
        }
        $user->avatar = $avatar;
        $user->background = $background;

        return $user;
    }

    //----------------------------------------Helpers----------------------------------------
}

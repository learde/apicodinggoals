<?php

namespace App\Http\Controllers;
use Cloudinary;
use Illuminate\Http\Request;


class ImageUploadController
{
     static public function uploadImages($file, $path, $name)
    {
         $result = $file->storeOnCloudinary($path, $name)->getPath();
         return $result;
    }
     static public function destroy_image($path)
   {
        Cloudinary::destroy($path);
   }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use App\Models\User;

use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Webup\LaravelSendinBlue\SendinBlue; 
use Webup\LaravelSendinBlue\SendinBlueTransport;


class UserRegistered extends Mailable
{
    use Queueable, SerializesModels, SendinBlue;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view('emails.confirm')->subject(SendinBlueTransport::USE_TEMPLATE_SUBJECT)->sendinblue([]);
     //  
    }
}

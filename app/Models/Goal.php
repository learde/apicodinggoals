<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Goal extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'title',
        'endCriterion',
        'description',
        'todo',
        'isPrivate',
        'isCommentable',
        'user_id',
        'image',
        'repo',
        'isFinished',
        'category'
    ];

   


    public function user()
    {
        return $this->BelongsTo(User::class);
    }

    public function posts()
    {
        return $this->HaMany(Post::class);
    }
   


}

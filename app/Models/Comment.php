<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;


   
    protected $fillable = [
        'id',
        'text',
        'likes',
        'user_id',
        'post_id',
        
        
    ];

    protected $attributes = [
        'likes' => "{}",
    ];

    protected $casts = [
        'likes' => 'array',
    ];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }


    
    public function user()
    {
        return $this->BelongsTo(User::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class post extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'text',
        'goal_id',
        'likes',
    ];

    protected $attributes = [
        'likes' => "{}",
    ];

    protected $casts = [
        'likes' => 'array',
    ];
    
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
    public function goal()
    {
        return $this->belongsTo(Goal::class);
    }
}

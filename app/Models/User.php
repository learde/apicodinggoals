<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;

use Tymon\JWTAuth\Contracts\JWTSubject;


class User extends Authenticatable implements JWTSubject, MustVerifyEmail
{
    use HasFactory, Notifiable;

    public static function boot()
    {
        parent::boot();
     
        static::creating(function ($user) {
            $user->token = Str::random(30);
        });
    }

    // public function setPasswordAttribute($password)
    // {
    //     $this->attributes['password'] = bcrypt($password);
    // }

    public function confirmEmail()
    {
        
        $this->verified = true;
        $this->token = null;

        $this->save();
        return 1;
    }



    public function goals()
    {
        return $this->hasMany(Goal::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function messages()
    {
        return $this->hasMany(Message::class, 'sender_id')->union($this->hasMany(Message::class, 'recipient_id'));
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'country',
        'town',
        'birthday',
        'surname',
        'subscriptions',
        'favorites',
        'patro',
        'NewPassword',
        'avatar',
        'background',
        'git',
        'description',
        'token', 
        'verified',
        'subscribers',
        'likes'
    ];

   

    protected $attributes = [
        'subscriptions' => "{}",
        'subscribers' => "{}",
        'favorites' => "{}"
    ];

  
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'favorites' => 'array',
        'subscriptions' => 'array',
        'subscribers' => 'array',
    ];
    
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier() {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims() {
        return [];
    }    
}
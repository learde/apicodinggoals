<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => 'api','prefix' => 'auth'], function ($router) {

    Route::post('/login', [AuthController::class, 'login'])->name('login');
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);
    Route::get('/register/confirm/{token}', 'AuthController@confirmEmail');



});




Route::group(['middleware' => 'api', 'prefix' => 'profile'], function ($router) {
    Route::get('/item', 'MainController@userProfile');
    Route::get('/getuser/{id}', 'MainController@get_user_by_id');
    Route::delete('/deleteuser/{id}', 'MainController@del_user');

    Route::post('/edit', "MainController@EditProfile");
    Route::post('/addtosub/{user_id}', 'MainController@add_to_subscriptions');
    Route::post('/deletefromsub/{user_id}', 'MainController@del_from_subscriptions');
    Route::get('/allsub', 'MainController@all_subscriptions');
    Route::get('/allsubscribers', 'MainController@all_subscribers');
});


Route::group(['middleware' => 'api', 'prefix' => 'goals'], function ($router) {
    Route::get('/allitems/{limit}', 'GoalController@getAll');
    Route::get('/useritems/{limit}', 'GoalController@getOneUserGoals');
    Route::get('/useritems/{user_id}/{limit}', 'GoalController@getOneUserGoals_id');
    Route::get('/item/{id}', 'GoalController@getOne');

    Route::post('/create', 'GoalController@create');
    Route::post('/create/{count}', 'GoalController@create_n_goals');
    Route::post('/edit/{id}', 'GoalController@Edit');
    Route::delete('/delete/{id}', 'GoalController@delete');
    Route::post('/search/{limit}', 'GoalController@search');

    Route::post('/addtofav/{goal_id}', 'GoalController@add_to_favorites');
    Route::post('/deletefromfav/{goal_id}', 'GoalController@del_from_favorites');
    Route::get('/allfav', 'GoalController@all_favorites');

});


Route::group(['middleware' => 'api', 'prefix' => 'posts'], function ($router) {
    Route::post('/create/{goal_id}', 'PostController@create');
    Route::delete('/delete/{post_id}', 'PostController@delete');
    Route::post('/edit/{post_id}', 'PostController@edit');

    Route::post('/comment/{post_id}', 'PostController@comment');
    Route::delete('/comment/delete/{comm_id}', 'CommentController@delete');
    Route::post('/comment/edit/{comm_id}', 'PostController@comment');

    Route::get('/allcomments/{post_id}/{limit}', 'PostController@get_comments');
    Route::get('/allposts/{goal_id}/{limit}', 'PostController@get_posts');

    Route::post('/like/{post_id}', 'PostController@like');
    Route::post('/unlike/{post_id}', 'PostController@unlike');
    Route::get('/alllikes/{post_id}', 'PostController@get_likes');
    Route::post('/comment/like/{comm_id}', 'CommentController@like');
    Route::post('/comment/unlike/{comm_id}', 'CommentController@unlike');
    Route::post('/comment/alllikes/{comm_id}', 'CommentController@get_likes');

});


Route::group(['middleware' => 'api', 'prefix' => 'messages'], function ($router) {

    Route::post('/create/{user_id}', 'MessageController@create');
    Route::delete('/delete/{id}', 'MessageController@delete');
    Route::post('/edit/{id}', 'MessageController@edit');
    Route::get('/showmessages/{user_id}', 'MessageController@show_messages');
    Route::get('/showdialogs', 'MessageController@show_dialogs');
});


Route::post('/webhook', [\App\Http\Controllers\MessageController::class, 'webhook']);

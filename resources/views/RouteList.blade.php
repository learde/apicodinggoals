
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <link rel="stylesheet" href="{{ secure_asset('/css/round.css')}}">
        <link rel="stylesheet" href="/css/header.css">
    </head>

<body>

    <div class="header-h1"> 
        <h1>Auth</h1> 
    </div>


    <ol class="square">
        <li>POST::<a href="#">api/auth/login</a> - (email, password)</li>
        <li>POST::<a href="#">api/auth/register</a> - (name, emai, password, password_confirmation)</li>
        <li>POST::<a href="#">api/auth/logout</a></li>
        <li>POST::<a href="#">api/auth/refresh</a></li>
    </ol>

    <div class="header-h1"> 
        <h1>Goals</h1> 
    </div>

    
    <ol class="square">
        <li>POST::<a href="#">api/goals/create</a> - (image) - создание цели</li>
        <li>DELETE::<a href="#">api/goals/delete/{id}</a> - удаление цели</li>
        <li>POST::<a href="#">api/goals/edit/{id}</a> - (image) - редактирование цели</li>
        <li>GET::<a href="#">api/goals/item/{id}</a> - получить обьект цели</li>
        <li>GET::<a href="#">api/goals/useritems/{limit}</a> - все цели юзера</li>
        <li>GET::<a href="#">api/goals/useritems/{user_id}/{limit}</a> - все цели юзера по id</li>
        <li>GET::<a href="#">api/goals/allitems/{limit}</a> - все цели</li>
        <li>POST::<a href="#">api/goals/search/{limit}</a> - (title) - поиск цели</li>
        <li>POST::<a href="#">api/goals/addtofav/{goal_id}</a> - добавить в избранные цели</li>
        <li>POST::<a href="#">api/goals/deletefromfav/{goal_id}</a> - удалить из избранных</li>
        <li>GET::<a href="#">api/goals/allfav</a> - все избранные</li>
    </ol>

    <div class="header-h1"> 
        <h1>Posts</h1> 
    </div>

    <ol class="square">
        <li>POST::<a href="#">api/posts/create/{goal_id}</a> - создать пост</li>
        <li>DELETE::<a href="#">api/posts/delete/{post_id}</a> - удалить пост</li>
        <li>POST::<a href="#">api/posts/edit/{post_id}</a> - редактировать пост</li>
        <li>POST::<a href="#">api/posts/comment/{post_id}</a> - (text) - прокомментировать пост</li>
        <li>POST::<a href="#">api/posts/comment/delete/{comm_id}</a> - (text) - удалить коммент</li>
        <li>POST::<a href="#">api/posts/comment/edit/{comm_id}</a> - (text) - редактировать коммент</li>
        <li>POST::<a href="#">api/posts/like/{post_id}</a> - поставить лайк посту</li>
        <li>POST::<a href="#">api/posts/unlike/{post_id}</a> - убрать лайк с поста</li>
        <li>GET::<a href="#">api/posts/alllikes/{post_id}</a> - вывести все лайки поста</li>
        <li>POST::<a href="#">api/posts/comment/like/{comm_id}</a> - поставить лайк комменту</li>
        <li>POST::<a href="#">api/posts/comment/unlike/{comm_id}</a> - убрать лайк с коммента</li>
        <li>GET::<a href="#">api/posts/comment/get_likes/{comm_id}</a> - вывесьи все лайки коммента</li>
        <li>GET::<a href="#">api/posts/allcomments/{post_id}/{limit}</a> - (text) - вывести все комментарии</li>
        <li>GET::<a href="#">api/posts/allposts/{goal_id}/{limit}</a> - вывести все посты</li>    
    </ol>


    <div class="header-h1"> 
        <h1>Profile</h1> 
    </div>



    <ol class="square">
        <li>POST::<a href="#">api/profile/edit</a> - (avatar, background - фото) - редактировать профиль</li>
        <li>GET::<a href="#">api/profile/item</a> - получить обьект юзера</li>
        <li>POST::<a href="#">api/profile/addtosub/{user_id}</a> - добавить юзера в подписки</li>
        <li>POST::<a href="#">api/profile/deletefromsub/{user_id}</a> - удалить из подписок</li>
        <li>GET::<a href="#">api/profile/getuser/{id}</a> - получить юзера по айди</li>
       
       
    </ol>



    <div class="header-h1"> 
        <h1>Messages</h1> 
    </div>



    <ol class="square">
        <li>POST::<a href="#">api/messages/create/{user_id}</a> - (message) - отправить сообщение</li>
        <li>DELETE::<a href="#">api/messages/delete/{id}</a> - удалить сообщение по айди</li>
        <li>POST::<a href="#">api/messages/edit</a> - (message) - редактировать сообщение</li>
        <li>GET::<a href="#">api/messages/showmessages/{user_id}</a> - получить все сообщения</li>
        <li>GET::<a href="#">api/messages/showdialogs</a> - получить все диалоги</li>
       
       
    </ol>

</body>

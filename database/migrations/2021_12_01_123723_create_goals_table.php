<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGoalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goals', function (Blueprint $table) {
            $table->id();
            $table->timestamps();


            $table->string('title')->default('');
            $table->string('endCriterion')->default('');
            $table->string('repo')->default(' ')->nullable();
            $table->string('image')->default('https://res.cloudinary.com/dme9p3izd/image/upload/v1648937213/apicodinggoals/avatars/goals/pe6f1qqpiiaqetevh423.png');
        

            $table->text('description')->nullable()->default(' ');
            $table->text('todo')->nullable();


            $table->boolean('isPrivate')->default(0);
            $table->boolean('isCommentable')->default(0);
            $table->boolean('isFinished')->default(0);


            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');


            $table->string('category')->default('No category')->nullable();

           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goals');
    }
}

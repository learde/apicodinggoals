<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();


            $table->string('name');
            $table->string('surname')->default('');
            $table->string('patro')->default('');
            $table->string('country')->default('');
            $table->string('town')->default('');
            $table->string('birthday')->default('');
            $table->string('gender')->default('');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('token')->nullable();   
            $table->string('NewPassword')->default('');
            $table->string('git')->default(' ')->nullable();


            $table->text('description')->nullable()->default(' ');
            $table->timestamp('email_verified_at')->nullable();
            $table->boolean('verified')->default(false);

            $table->json('subscriptions');
            $table->json('subscribers');
            $table->json('favorites');
            $table->integer('likes')->nullable();
         
            $table->string('background')->default('http://res.cloudinary.com/dme9p3izd/image/upload/v1648404126/apicodinggoals/backgrounds/defaults/ulxxheiu8emsho3aa3g4.jpg');
            $table->string('avatar')->default('http://res.cloudinary.com/dme9p3izd/image/upload/v1648404630/apicodinggoals/avatars/users/idw5p3a8nmpx0issug7z.webp');


            $table->rememberToken();
            $table->timestamps();

            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
